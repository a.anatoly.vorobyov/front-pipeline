import React from "react";
import NavBar from "./NavBar";
import Content from "./Content";
import SearchBar from "./SearchBar";
import "./Page.css"

class Page extends React.Component {
    render() {
        return (
            <div className="Page">
                <NavBar/>
                <div className="Main">
                    <Content/>
                    <SearchBar/>
                </div>
            </div>
        );
    }
}

export default Page;
