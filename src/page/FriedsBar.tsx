import React from "react";
import {Friend, FriendsService} from "../services/FriendsService";
import "./FriendBar.css";
import addIcon from "../icons/add.svg";

type Props = { friendsService?: FriendsService }
type State = { friends: Friend[] }

class FriendsBar extends React.Component<Props, State> {
    private readonly friendsService: FriendsService;

    constructor(props: Props) {
        super(props);
        this.friendsService = props.friendsService ? props.friendsService : new FriendsService();
        this.state = {
            friends: []
        }
    }

    componentDidMount(): void {
        this.friendsService.getFriends({limit: 4})
            .then(friends => {
                this.setState({friends})
            })
    }

    render() {
        return (
            <div className="FriendBar">
                <div className="FriendBlock">
                    <div>
                        <img className="FriendImage AddImage" src="https://m.media-amazon.com/images/M/MV5BMjM2OTkyNTY3N15BMl5BanBnXkFtZTgwNzgzNDc2NjE@._V1_CR132,0,761,428_AL_UY268_CR82,0,477,268_AL_.jpg"/>
                    </div>

                    <p className="FriendName">You</p>
                </div>
                {this.state.friends.map(({firstName, lastName, avatar}) => {
                    return (
                        <div className="FriendBlock">
                            <img className="FriendImage" src={avatar}/>
                            {lastName.length < 10 ?
                                <p className="FriendName">{firstName} {lastName}</p> :
                                <p className="FriendName">{firstName}</p>
                            }
                        </div>
                    )
                })}
            </div>
        );
    }
}

export default FriendsBar;
