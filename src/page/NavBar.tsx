import React, {BaseSyntheticEvent, Component} from "react";
import './NavBar.css';
import Icon from "../elements/Icon";
import avatar from "../icons/avatar.svg";

import home from "../icons/home.svg";
import activeHome from "../icons/active/home.svg";

import message from "../icons/message.svg";
import activeMessage from "../icons/active/home.svg";

import person from "../icons/person.svg";
import activePerson from "../icons/active/person.svg";

import bookmark from "../icons/bookmark.svg";
import setting from "../icons/setting.svg";

type State = { activeMenu: string }
class NavBar extends Component<any, State> {
    constructor(props: any) {
        super(props);
        this.state = {activeMenu: 'home'}
    }

    onChangeMenu = (menu: BaseSyntheticEvent) => {
        console.log(menu.target)
        this.setState({activeMenu: menu.target.alt})
    };

    render() {
        return (
            <div className="NavBar">
                <div className="AvatarIcon">
                    <Icon iconSrc={avatar}/>
                </div>

                <div className="FullMenu">
                    <div className="MainMenu">
                        <Icon onClick={this.onChangeMenu}
                              alt="home"
                              iconSrc={this.state.activeMenu === 'home' ? activeHome : home}/>
                        <Icon onClick={this.onChangeMenu}
                              alt="message"
                              iconSrc={this.state.activeMenu === 'message' ? activeMessage : message}/>
                        <Icon onClick={this.onChangeMenu}
                              alt="person"
                              iconSrc={this.state.activeMenu === 'person' ? activePerson : person}/>
                        <Icon onClick={this.onChangeMenu}
                              alt="bookmark"
                              iconSrc={bookmark}/>
                    </div>
                    <div className="SettingMenu">
                        <Icon iconSrc={setting}/>
                    </div>
                </div>
            </div>
        )
    }
}

export default NavBar;
