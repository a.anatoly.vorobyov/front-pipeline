import React from 'react';
import './Content.css';
import FriendsBar from "./FriedsBar";

class Content extends React.Component {
    render() {
        return (
            <div className="Content">
                <FriendsBar/>
            </div>
        );
    }
}

export default Content;
