import React, {Component} from "react";
import "./Icon.css";

type Props = { iconSrc: string, alt?: string, onClick?: (value: any) => void }

class Icon extends Component<Props> {
    render() {
        return (
            <img
                onClick={this.props.onClick}
                src={this.props.iconSrc}
                className="Icon" alt={this.props.alt || "icon"}
            />
        );
    }
}

export default Icon;
