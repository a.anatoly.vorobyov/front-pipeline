export interface Friend {
    readonly firstName: string;
    readonly lastName: string;
    readonly avatar: string;
}

type GetQuery = { limit: number, offset?: number, search?: string };

export class FriendsService {
    public async getFriends({limit, offset = 0} : GetQuery): Promise<Friend[]> {
        return [
            {firstName: "test", lastName: "test", avatar: "https://m.media-amazon.com/images/M/MV5BMjM2OTkyNTY3N15BMl5BanBnXkFtZTgwNzgzNDc2NjE@._V1_CR132,0,761,428_AL_UY268_CR82,0,477,268_AL_.jpg"},
            {firstName: "test1", lastName: "", avatar: "https://m.media-amazon.com/images/M/MV5BMjM2OTkyNTY3N15BMl5BanBnXkFtZTgwNzgzNDc2NjE@._V1_CR132,0,761,428_AL_UY268_CR82,0,477,268_AL_.jpg"},
            {firstName: "test2", lastName: "", avatar: "https://m.media-amazon.com/images/M/MV5BMjM2OTkyNTY3N15BMl5BanBnXkFtZTgwNzgzNDc2NjE@._V1_CR132,0,761,428_AL_UY268_CR82,0,477,268_AL_.jpg"},
            {firstName: "test3", lastName: "fsdsdfsfsdfsdfsdfs", avatar: "https://m.media-amazon.com/images/M/MV5BMjM2OTkyNTY3N15BMl5BanBnXkFtZTgwNzgzNDc2NjE@._V1_CR132,0,761,428_AL_UY268_CR82,0,477,268_AL_.jpg"},
            {firstName: "test5", lastName: "", avatar: "https://m.media-amazon.com/images/M/MV5BMjM2OTkyNTY3N15BMl5BanBnXkFtZTgwNzgzNDc2NjE@._V1_CR132,0,761,428_AL_UY268_CR82,0,477,268_AL_.jpg"},
            {firstName: "test6", lastName: "", avatar: "https://m.media-amazon.com/images/M/MV5BMjM2OTkyNTY3N15BMl5BanBnXkFtZTgwNzgzNDc2NjE@._V1_CR132,0,761,428_AL_UY268_CR82,0,477,268_AL_.jpg"},
        ]
    }
}

